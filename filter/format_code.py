#/usr/local/bin/python
"""
Pandoc filter using panflute
"""

import panflute as pf
import sys


def prepare(doc):
    pass

def action(elem, doc):

    #check for headers and when to turn on off keep options
    if isinstance(elem,pf.CodeBlock) and pf.stringify(elem).startswith("```"):
        lines = elem.text.split("\n")
        elem.classes=[lines[0].strip("```")]
        elem.text = "\n".join(lines[1:])

    return elem 

def finalize(doc):
    pass


def main(doc=None):
    return pf.run_filter(action,
                         prepare=prepare,
                         finalize=finalize,
                         doc=doc,
                         ) 


if __name__ == '__main__':
    main()


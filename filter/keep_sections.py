#/usr/local/bin/python
"""
Pandoc filter using panflute
"""

import panflute as pf
import sys


def prepare(doc):
    doc.to_delete = True   
    doc.metadata['weight'] = 4
    doc.description = False
    pass

def action(elem, doc):
    #check for headers and when to turn on off keep options
    if isinstance(elem,pf.Header) and elem.level==1:
        if pf.stringify(elem).startswith(doc.get_metadata('keep')):
            doc.to_delete = False
            return []
        else:
            doc.to_delete = True
    if isinstance(elem,pf.Header) and elem.level==2:
        if pf.stringify(elem).startswith("Functional Requirements"):
            doc.to_delete = True

    if doc.to_delete and isinstance(elem,pf.Block):
        return [] 
    else:
        return elem 


def finalize(doc):
    pass


def main(doc=None):
    return pf.run_filter(action,
                         prepare=prepare,
                         finalize=finalize,
                         doc=doc,
                         ) 


if __name__ == '__main__':
    main()


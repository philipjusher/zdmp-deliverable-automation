---
title: "Data Harmonisation Designer"
linkTitle: "Data Harmonisation Designer"
date: 2020-05-05
weight: 4
description: >
  The Data Harmonisation Designer allows developers to define data maps which take data from existing software systems and restructure it for the needs of other systems. This component works in conjunction with the run-time component, T5.3: Data Harmonisation Run-time.
---

| Resource                                          | Location          |
|---------------------------------------------------|-------------------|
| Source Code                                       | [Link](https://zdmp-gitlab.ascora.eu/zdmp_code/developer-tier/t5.3-data-harmonization-designer)          |
| Latest Release                                    | Vs 1.0.0       |
| X Open API Spec                                   | [Link](https://software.zdmp.eu/docs/openapi/developer-tier/data-harmonisation-designer/harmonization-api.yaml/)       |
| Video                                             | Coming soon       |

## General description

This component has access to raw data and ensures that data can be integrated using unified and standardised formats or the formats needed by the data recipient. It also provides basic functionalities for semantic homogenisation in a context of heterogeneous data meaning that the mapping is facilitated with reference to semantic though a ZDMP ontology. 

The developed application enables a business analyst driven approach for the automatic linking of organisations’ data and meta data to the reference data model. 

These data Maps are available in the ZDMP Data Storage and deployed and encapsulated as services to be finally exposed as software mini-packages, ie Docker containers. These mini-packages, containing the transformation routines, are uploaded, and published in the Marketplace to advertise and commercialise them. 

One of their uses is, for example, as part of the execution of a process model in which they are rendered as services that can be called.

## Architecture Diagram

The following diagram shows the position of this component in the ZDMP architecture.

![Figure 1: Position of the Data Harmonisation Designer and Run-Time in ZDMP Architecture](/img/developer-tier/data-harmonisation-designer/5.3_data_harmonisation_architecture_diagram.png)

## Features

* **Maps Designer**
* **Semantic Reasoner** 
* **Ontology Storage**
* **Data Map Parser**
* **Data Enrichment and Feature Extraction API**
* **Map Designer UI** 
* **Publish Map API**
* **Load and Save API**
* **Schema Analyser API**

### Maps Designer

The Maps Designer module allows a person to generate Manufacturing Maps. A Manufacturing Map file describes the rules to be executed to transform a specific syntax format A into format B which could then, for example, be used as part of a process. 

This map is a java archive file (with the .jar extension) wrapped in a docker container for execution. This container executes as a transformation engine. 

It offers the developer the possibility to annotate these maps with additional descriptions. 

This description eases the search and selection of the appropriate mapping rules. Manufacturing Maps can be stored in T6.2 Storage and may also be sent to the T6.2 Marketplace to use them as a service in T5.4 Orchestration Designer / Run-time, or via API requests from the T6.4 Service and Message Bus.

### Semantic Reasoner

Provides the user with helpful suggestions when performing mappings between distinct concept knowledge bases. The component relies on user input to improve suggestion quality, as it incorporates the effective mappings elected by users into a growing internal knowledge base. 

It supports mapping restrictions for specific subsets of source and target mappings and provides data versatility by supporting different conceptual types of knowledge base transparently – triples, graph, ontology, or tabular.

### Ontology Storage

Enables persistent data storage in support of the Semantic Reasoner. It is responsible for holding the relevant ontology knowledge bases in graph form as well as the overarching ZDMP ontology. 

The ZDMP ontology contains ZDMP specific vocabulary and ontology alignment.

### Data Map Parser

It is the interoperability layer between the Semantic Reasoner core functionality and the Map Designer. It is responsible for transforming generic requests into Semantic Reasoner queries.

### Data Enrichment and Feature Extraction API

Derive attributes and features from the data. Use statistical properties, data models, and temporal data characteristics to discover internal relationships within the data.

### Map Designer UI

This UI is based upon the current ICE Data Platform (IDP). Developers can access the various Mapping features in a friendly way, eg generate Manufacturing Maps or annotate them with keywords and metadata. 

It also allows the connection with the Data Service components to retrieve Data from querying the data model, ie the ontology.

### Publish Map API

This interface encapsulates the access to the T6.2 Marketplace, where the deployed Maps are stored for re-use to sell and share.

The maps may also be published to T6.1 Application Builder for integration into zApps and directly to the T5.3 Data Harmonisation Run-time.

### Load and Save API

This interface encapsulates the access to the Storage, where data from this component is stored.

### Schema Analyser

This interface manages the actual access to the external data sources and the objects where the data is generated and/or stored. 

These formats can range from a text-file to a database schema passing by a CSV-type file or an XML and JSON types. 

This helps to facilitate inter-platform interoperability by taking example data and meta-data from external platforms.

## System Requirements

Minimum requirements needed:

* *Docker*

## How to use

This section will show how to use the Data Harmonisation Component, with the following sections:

* **Maps Designer**
* **Semantic Reasoner** 
* **Ontology Storage**
* **Data Map Parser**
* **Data Enrichment and Feature Extraction API**
* **Map Designer UI** 
* **Publish Map API**
* **Load and Save API**
* **Schema Analyser API**

### The Welcome Page

When the user runs the file TOS_DI-winx86_64.exe, the ICE Data Platform opens. The user is prompted to either run an existing project, or create a new project. After they name a new project, they’ll be taken to same screen as in the screenshot below.

![Figure 2: Screenshot of the Ice Data Platform Welcome Page](/img/developer-tier/data-harmonisation-designer/how_to_1_welcome_page.png)

From here, the user can open a job they have previously worked on, or they can create a new job. They can also be directed towards various tutorials where they can learn more about the intricacies of the Ice Data Platform, or view more documentation.

### Creating a New Job

When the user chooses to create a job, they are greeted with a popup box that requires the user to enter. Whilst the Name field is required, there are warnings about leaving the Purpose and Description fields blank. The user cannot continue until they enter a name, but the other details can be changed through the properties menu.

![Figure 3: A Screenshot Showing the New Job Pop-up Box](/img/developer-tier/data-harmonisation-designer/how_to_2_new_job_pop_up.png)

When the user does select the finish button, the pop-up box disappears and the user views the main canvas of the ICE Data Platform. On the left side of the screen, they can see the list of jobs, as well as the code base that makes up the different jobs. On the right, they can see the palette of tasks that they can use. In Figure 3, the palette has not loaded, and this process can take a few seconds before it becomes populated with elements.

![Figure 4: A Screenshot of the ICE Data Platform with a Populated Palette](/img/developer-tier/data-harmonisation-designer/how_to_3_IDP_palette.png)

### Creating Inputs and Outputs

From the open version of the ICE Data Platform, users can click and drag elements from the palette into the canvas to create jobs. Eventually, these jobs will be exported to become executable services.

From the Palette, the user can search for specific tasks, such as importing an Excel file (tFileInputExcel), exporting to a text file (tFileOutputDelimited) or use the Semantic Reasoner (tSemanticMap). 

![Figure 5: A Screenshot Showing the User Using the Canvas](/img/developer-tier/data-harmonisation-designer/how_to_4_canvas_use.png)

As shown in Figure 5, the tFileInputExcel_1 task has a red exclamation symbol on it. This shows the user that they have actions to do on the task. For this task specifically, it is defining the schema of the input, and whilst this is for an Excel input, the process is similar for most inputs. 

![Figure 6: A Screenshot showing the File Input Properties](/img/developer-tier/data-harmonisation-designer/how_to_5_file_input_properties.png)

As shown in Figure 6, the user will be required to define the files that they want the ICE Data Platform to use. This process will be the same for both input and output processes. The user defines what they are putting into the process, and they define what they expect to get out of it. 

### Semantic Reasoner

The functionality that the ICE Data Platform can offer includes the ability to transform files between formats. The way that it does this is by using the Semantic Reasoner component, or in the palette, the tSemanticMap task. 

![Figure 7: A Screenshot of the Ice Data Platform Canvas using the Semantic Reasoner](/img/developer-tier/data-harmonisation-designer/how_to_6_semantic_reasoner_on_canvas.png)

By double clicking on the tSemanticMap_1, they are taken to the Semantic Reasoner interface.

![Figure 8: The Ice Data Platform Semantic Reasoner Interface](/img/developer-tier/data-harmonisation-designer/how_to_7_semantic_reasoner_interface.png)

From this screen, the user can see the schema of both input and outputs to the semantic mapping task. As in Figure 8, the bottom right, there is the “Auto map!” which will use the  Semantic Reasoner component to make the best guesses at which of the input data will match to the same or similar output data.

![Figure 9: The Semantic Reasoner Mapping to a Smaller Output](/img/developer-tier/data-harmonisation-designer/how_to_8_semantic_reasoner_output.png)

The user can also make modifications here, and this will send data back to the Semantic Reasoner to help improve the semantic mapping in future. 

When they select the apply button, these changes will be applied, and when they click the ok button, they will be taken back to the canvas. 

### Building a Job

When the user has created a job, they can build the job into an executable process. This process can be used in conjunction with other executable processes and services. 

![Figure 10: A Screenshot of the Build Job Pop-up Box](/img/developer-tier/data-harmonisation-designer/how_to_9_build_job_pop_up.png)

As shown in Figure 10, the user is given various options. From where to save built job, to more specific options, like the different shell options. One of the more important options, is the Build Type selection, where the user can get choose the build type, choosing between a standalone job, or a ZDMP Docker instance. 

This Docker instance can be utilised by any of the components that use Docker, and if they don’t, or the user doesn’t use the Docker instances, they can choose the standalone executable and they can utilise it however they choose.

## Functional Requirements M18 Status
This component looks to build and execute data maps which transform data from one file format to another.


| Functional requirement 	| Description           	| Status      	| Progress       	| Comments   	|
|------------------------	|-----------------------	|-------------	|----------------	|------------	|
| T53A001 Connect to Database                	| Open filesystem and access the schema of the data source (both for source and target schemas) So that the schema can be loaded and, thus, the mappings can be performed         	| finished    	| 100     	|  	|
| T53A002 Read XML                	| interprets eg XML schema files So that the schema can be loaded and, thus, the mappings can be performed         	| finished    	| 100     	    |  	|
| T53A003 Read CSV                	| Interprets CSV schema files So that the Transformation engine can read the transformation steps determined in the Map         	| finished    	| 100 	|  	|
| T53A004 Read JSON                	| Interprets JSON schema files So that the Transformation engine can read the transformation steps determined in the Map         	| finished    	| 100  	|  	|
| T53A005 Read TXT                	| Interprets plain text schema files So that the Transformation engine can read the transformation steps determined in the Map      | finished    	| 100   |  	|
| T53A006 Read XLS                	| Interprets XLS schema files So that the Transformation engine can read the transformation steps determined in the Map         	| finished    	| 100 	|  	|
| T53A007 Read MySQL                	| Interprets MySQL schema files So that the Transformation engine can read the transformation steps determined in the Map       | finished    	| 100  	|  	|
| T53A008 Display UI                	| Show Mapping UI so that the source schema can be displayed and, thus, the mappings can be performed         	| finished    	| 100     	    |  	|
| T53A009 Load Source Schema                	| Loads source schema So that the source schema can be accessed         	| finished    	| 100     	|  	|
| T53A010 Display Source Schema                	| Display source schema So that the source schema can be viewed         	| finished    	| 100     	|  	|
| T53A011 Analyse Source Schema                	| Analyses source schema So that the source schema can be manipulated         	| testing    	| 75     	|  	|
| T53A012 Connect to ontology                	| Connects to domain (or ZDMP) ontology So that alternative concepts can be suggested for the concepts present in the source schema         	| testing    	| 70     	|  	|
| T53A013 Suggest Semantic Concepts for Source Schema                	| Suggests alternative (or linked) concepts to the concepts present in the source schema So that a crowdsourced domain (or ZDMP) ontology can be populated         	| testing    	| 75     	| The processes are in place, but we still are improving the concepts. 	|
| T53A014 Display UI                	| Show Mapping UI So that the target schema can be displayed and, thus, the mappings can be performed         	| finished    	| 100     	|  	|
| T53A015 Load Target Schema                	| Loads target schema So that the target schema can be accessed         	| finished    	| 100     	|  	|
| T53A016 Display Target Schema                	| Display target schema So that the target schema can be viewed         	| finished    	| 100     	|  	|
| T53A017 Analyse Target Schema                	| Analyses target schema So that the target schema can be manipulated         	| testing    	| 75     	|  	|
| T53A018 Connect to Ontology                	| Connects to domain (or ZDMP) ontology So that alternative concepts can be suggested for the concepts present in the target schema         	| testing    	| 75     	|  	|
| T53A019 Suggest Semantic Concepts for Target Schema                	| Suggests alternative (or linked) concepts to the concepts present in the target schema So that a crowdsourced domain (or ZDMP) ontology can be populated         	| testing    	| 75     	|  	|
| T53A020 Connect to the Storage                	| Connect to the Storage with the credentials as directed by the T5.2 Secure Authentication/Authorisation component So that the Maps can be read, searched, and filtered after save         	| working    	| 50     	| The design time processes are in place, but we still need to work on the integration aspects. 	|
| T53A021 Read file                	| Read file So that the Maps can be loaded into the Data Harmonisation Designer component         	| finished    	| 100     	|  	|
| T53A022 Search Map                	| Search map So that the Maps can be searched into the Storage component         	| working    	| 50     	| The processes are in place, but we still need to work how we plan to store the maps, and work on the integration. 	|
| T53A023 Filtering in the Storage                	| Apply filters So that the Maps stored in the Data Storage can be filtered according to user specified criteria         	| working    	| 40     	| The processes are in place, but we still need to work how we plan to store the maps, and work on the integration. 	|
| T53A024 Preview Map                	| Preview map So that the Maps can be graphically previewed before loading into the Data Harmonisation Designer component         	| working    	| 30     	| The processes are in place, but we still need to work how we plan to store the maps, and work on the integration. 	|
| T53A025 Annotate Map                	| Annotate map with metadata So that the Maps can be searched and filtered with this metadata as parameters         	| working    	| 60     	| Maps can be annotated, but still needs development to know that it can work during the runtime. 	|
| T53A026 Publish Map                	| The map is published to a computer readable format So that the Maps can be wrapped and become an executable service         	| working    	| 40     	| The design time can publish the maps, but the runtime publishing is still under development. 	|
| T53A027 Persist Map                	| Save the map in the Storage So that the Maps can be re-used, retrieved, removed, searched, and filtered         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A028 Search Map                	| Search map So that the Maps can be searched into the Storage component         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A029 Delete Map                	| Remove map Checks that the persisted Maps in the Storage can be removed from it         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A030 Annotate Service                	| Annotate map for publication So that the to-be deployed Map can be easily found in the Storage         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A031 Create Service                	| Create self-executing service So that the deployed Map can be executed as a stand-alone service         	| testing    	| 90     	|  	|
| T53A032 Deploy Service                	| Create Docker package So that the deployed Map can be executed and scalable if necessary, as a stand-alone service         	| testing    	| 90     	|  	|
| T53A033 Connect to the Storage                	| Connect to Marketplace with the credentials as directed by the T5.2 Secure Authentication/Authorisation component So that the Transformation Services (ie the deployed maps) can be published         	| working    	| 50     	| All the processes are there, will be finalised when during the integration process.  	|
| T53A034 Publish Deployed Map                	| Publish deployed map (ie transformation service) So that the Transformation Service can be sold to ZDMP Users and re-used within eg the Process Orchestration         	| working    	| 50     	| All the processes are there, will be finalised when during the integration process.  	|
| T53A035 Get Related Data                	| Obtain data with internal relationships to a given concept passed as parameter So that the Data Harmonisation Designer can make use of the "wisdom of the crowd" for developing the maps         	| working    	| 30     	| The design time portion of this requirement is in place, but the runtime is still in progress. 	|
| T53A036 Get Routine                	| Obtain the internal relationships between a set of given concepts passed as parameters So that the Data Harmonisation Designer can make use of the "wisdom of the crowd" for developing the maps         	| working    	| 30     	| Development on the relationship ontologies are ongoing.  	|
| T53A037 Add Related Data                	| Add a new concept that is linked to a given concept passed as parameter So that the Data Harmonisation Designer can provide feedback to the "wisdom of the crowd" for developing future maps         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A038 Add Routine                	| Add a new link between two given concepts, passed as parameters So that the Data Harmonisation Designer can provide feedback to the "wisdom of the crowd" for developing future maps         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A039 Update Related Data                	| Update a concept that is linked to a given concept passed as parameter So that the Data Harmonisation Designer can provide feedback to the "wisdom of the crowd" for developing future maps         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A040 Update Routine                	| Update a given link between two concepts, passed as parameters So that the Data Harmonisation Designer can provide feedback to the "wisdom of the crowd" for developing future maps         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53A041 Get Concept                	| Get concepts from the ontology So that the Data Harmonisation Designer can make use of the ZDMP Ontology for developing the maps         	| working    	| 70     	| Concepts can be pulled, but development on the UI to view them is in progress. 	|
| T53A042 Add Concept                	| Add concepts to the ontology So that the Data Harmonisation Designer can make use of the ZDMP Ontology for developing the maps         	| working    	| 60     	| Concepts can be added, but the semantic reasoner integration process is still ongoing.  	|
| T53A043 Update Concept                	| Update concepts in the ontology So that the Data Harmonisation Designer can make use of the ZDMP Ontology for developing the maps         	| working    	| 30     	| The UI affects the concepts, so this process is worked on throughout the semantic reasoner integration. 	|
| T53A044 Delete Concept                	| Remove concepts from the ontology So that the Data Harmonisation Designer can update the ZDMP Ontology for future developing the maps         	| working    	| 60     	| Concepts can be deleted, but the semantic reasoner integration process is still ongoing. 	|
| T53A045 Reasoning                	| Reason So that the Data Harmonisation Designer can make use of the ZDMP Ontology for developing the maps         	| working    	| 90     	|  	|
| T53B001 Get invocation                	| Get invocation request from Service Call So that the transformation engine can execute the right transformation service         	| working    	| 50     	| The connection is working, but the execution process is still being developed.  	|
| T53B002 Connect to Store                	| Connect to ZDMP Store with the credentials as directed by the T5.2 Secure Authentication/Authorisation component So that the transformed data can be stored         	| working    	| 20     	| We are still working on how to store components, and integration. 	|
| T53B003 Unpack Routines                	| Unpack a mapping routine set So that the Transformation engine can read the transformation steps determined in the Map         	| working    	| 30     	| The runtime component is proving more difficult, more integration is needed. 	|
| T53B004 Read Data                	| Reads source data as input parameter from the invocation So that the input data can be transformed by executing the transformation services         	| working    	| 50     	| The design time equivalent of this is completed, but the runtime side of it has proven difficult and is still being developed.  	|
| T53B005 Transform                	| Transforms the data So that the routines of the mapping are executed         	| finished    	| 100     	|  	|
| T53B006 Push Transformed Data                	| Pushes transformed data back to the calling zApp So that the routines of the mapping are executed         	| working    	| 50     	| The processes are in place for the design time, but we are still developing the runtime aspects. 	|
| T53B007 Store Transformed Data                	| Store the transformed data So that the transformed data is accessible without having to re-execute the transformation service again         	| working    	| 50     	| The design time related side of this task are ready, the runtime . 	|
| T53B008 Submit Usage Data                	| Submit usage data So that the platform can make use of this data for monitoring purposes         	| working    	| 30     	| The processes are in place for the design time, but we are still developing the runtime aspects and integration. 	|

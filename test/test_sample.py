import os.path
import subprocess
import sys

import pytest
import panflute as pf
import filter.keep_sections as keep_sections

def count_headers(elem,doc):
    if isinstance(elem, pf.Header):
        doc.headers.append((elem.level,pf.stringify(elem)))

def run_pandoc(path,keepStr,fname,outputfile='output.md'):
    
    output = subprocess.run(['pandoc',
                    '-i', fname, 
                    '-f', 'docx',
                    '-M',f"keep={keepStr}",
                    '-F','./filter/keep_sections.py',
                    '-o',os.path.join(path,outputfile)],capture_output=True
                    )
    
    filePath = os.path.join(path,outputfile)
    assert output.returncode == 0  
    assert os.path.isfile(filePath)
    assert os.stat(filePath) != 0
    return output

def test_pandoc_ran(tmpdir):
    """Testing to see if it runs on the command line using example files"""
    tmpdir = './test'
    output = run_pandoc(tmpdir,'Keep','./test/test-sections.docx')
    print(output)
    assert output.returncode == 0

def test_fileCreation(tmpdir):
    "testing to see if it creates a file"
    run_pandoc(tmpdir,'Keep','./test/test-sections.docx')

def test_headers(tmpdir):
    outputfile = 'output.json'
    tmpdir = './test/'
    output = run_pandoc(tmpdir,'Keep','./test/test-sections.docx',outputfile=outputfile)
    headers = get_headers_json(tmpdir,outputfile,'Keep')
    for header in headers:
        assert headers[0] != 1
        if headers[0] == 1:
            assert headers[1] == 'To keep header 2'

def get_headers_json(tmpdir,outputfile,keepStr):
    with open(os.path.join(tmpdir,outputfile),'r', encoding = 'utf-8') as f:
        doc = pf.load(f)
    doc.headers = []
    doc.metadata['keep']=keepStr
    output = keep_sections.main(doc=doc)    
    
    output.walk(count_headers)
    return doc.headers
   
    
def test_dataharmonisation_headers(tmpdir):
    tmpdir = "./test/"
    outputfile = 'test-output.json'
    run_pandoc(tmpdir,'Component','./test/ZDMP.docx',outputfile=outputfile)
    headers = get_headers_json(tmpdir,outputfile,'Component:')
    print(headers)
    for header in headers:
        assert header[0] != 1
        if header[0] == 2:
            assert header[1] in ['General Description','Architecture Diagram','Features','System Requirements','How to use', 'Functional Requirements Implementation Status (M18)'] 

#def test_included_headers(tmpdir):
#    tmpdir = "./test/"
#    outputfile = 'zdmp-output.json'
#    run_pandoc(tmpdir,'Component','./test/ZDMP.docx',outputfile=outputfile)
#    headers = get_headers_json(tmpdir,outputfile,'Component:')
#    print(headers)
#    assert headers == 2
#

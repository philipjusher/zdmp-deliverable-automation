import logging
import fire
import yaml
from pathlib import Path
import subprocess
from datetime import date
class NotAbsoluteDirectory(Exception):
    """ Raised when the directory is not absolute 
    Attributes:
        directory -- the directory path which caused the error
        message -- explanation of the error

    """
    def __init__(self, directory, message="This path is not an absolute path"):
        self.directory=directory
        self.message=message
        super().__init__(self.message)
    

def checkPaths(directories):
    """checks a list of directories as strings into a list of paths and checks the paths exist and are directories and are full paths"""

    for directory in directories :
        directory = Path(directory)
        if not directory.exists():
            raise FileNotFoundError(directory) 
        if not directory.is_dir():
            raise NotADirectoryError(directory)
        if not directory.is_absolute():
            raise NotAbsoluteDirectory(directory)
    
def fileLoop(config,pandocconfig):
    i = 0
    for doc,component,tier,title in zip(config['inputFile'],config['componentName'],config['tier'],config['title']):
        i += 1
        
        logging.info(f'Procesing: {i}, {doc}, {tier}, {component}, {title}')
        
        pandocconfig['metadata']['keep'] = "Component: "+title 
        pandocconfig['metadata']['date'] =date.today().strftime("%Y-%m-%d")
        pandocconfig['metadata']['title'] = title 
        pandocconfig['metadata']['linktitle'] = title
        pandocconfig['extract-media'] = '/img/'+tier+'/'+component
        pandocconfig['output-file'] = '/components/'+tier+'/'+component+'.md'
        pandocconfig['input-files'] = ['/inputfile/'+doc]
        
        with open("data/pandocConfigs/pandocArgs"+component+".yaml","w") as f:
            yaml.safe_dump(pandocconfig,stream=f)
        subprocess.check_call(["pandoc", "-d","data/pandocConfigs/pandocArgs"+component+".yaml"])
def main(*, configPath,changedfile=None):
    """ Converts ZDMP word docs into the technical documentation markdown for Docsy Hugo template

    Keywork Arguments:
    configPath -- filepath to a config file
    """
    logging.basicConfig(level=logging.INFO,format='%(asctime)s - %(message)s',datefmt='%Y-%m-%d %H:%M:%S')   
    config = yaml.safe_load(open(configPath))
    pandocconfig = yaml.safe_load(open("data/pandocConfigs/pandocArgs.yaml"))
    #Basic santising of inpout
    checkPaths([config['techDocsDir'],config['wordDocsDir'],config['techImgDir']])
    config['inputFile'] = [] 
    config['tier'] = [] 
    config['componentName'] = [] 
    config['title'] = [] 
    for i in config['zdmpMap']:
        if i[0] == changedfile or changedfile == None:
            config['inputFile'].append(i[0]) 
            config['tier'].append(i[1]) 
            config['componentName'].append(i[2]) 
            config['title'].append(i[3]) 
    fileLoop(config, pandocconfig)
    
     
    

if __name__ == '__main__':
    fire.Fire(main)
    

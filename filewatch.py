import sys
import time
import logging
import os
from watchdog.observers.polling import PollingObserver
from watchdog.events import PatternMatchingEventHandler, FileSystemEventHandler
import zdmpdocs2md
class DocxEventHandler(FileSystemEventHandler):
#    def __init__(self):
#        super(DocxEventHandler,self).__init__(self, patterns=['*.docx'],ignore_directories=True, case_sensitive=False)

    def on_modified(self,event):
        logging.info(f"file modified{event.src_path}")

    def on_created(self,event):
        logging.info(f"file created{event.src_path}")
        self.process(event)

    def on_deleted(self, event):
        logging.info(f"file deleted {event.src_path}")

    def on_moved(self, event):
        #word saves files by using tmp files and moving to them 
        logging.info("file moved {event.src_path}")
        self.process(event)

    def process(self, event):
        #for some reason the move event triggers twice on the move from and move to
        if event.src_path.endswith('docx'): #pretty hacky
            changedfile = os.path.split(event.src_path)[-1]    
            
            logging.info(f"Identified {changedfile} for changing")
            zdmpdocs2md.main(configPath='config.yaml',changedfile=changedfile)            

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                                                format='%(asctime)s - %(message)s',
                                                datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    logging.info("hello")
    event_handler = DocxEventHandler()
    observer = PollingObserver()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

FROM python:3.8-slim-buster 
RUN apt-get update -y
RUN apt-get install wget -y
RUN wget https://github.com/jgm/pandoc/releases/download/2.9.2.1/pandoc-2.9.2.1-1-amd64.deb 
RUN dpkg -i pandoc-2.9.2.1-1-amd64.deb
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . /pandocDeliverables
RUN mkdir data
CMD python3 zdmpdocs2md.py --configPath config.yaml


#!/bin/bash


if [ $1 == "full" ]; then
	docker-compose up
elif [ $1 == "dev" ]; then
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml -f docker-compose-hugo.yaml up
elif  [ $1 == "bash" ]; then
	docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml run --rm pandoc /bin/bash 
elif [ $1 == "build" ]; then
	docker-compose build
else
   echo "Unknown Argument "$1""
   exit 1
fi

